from setuptools import setup

setup(name='azure-iothub-service-client',
      version='1.1.0.10',
      description='contains pre-built versions of the azure iot python sdk, all code from https://github.com/Azure/azure-iot-sdk-python',
      url='https://bitbucket.org/grandukeserver/fe_azure_iot_sdk_python',
      author='Jay Mikhail',
      author_email='jay.mikhail@farmersEdge.ca',
      license='MIT',
      packages=['iothub_service_client'],
      include_package_data=True,
      zip_safe=False)
